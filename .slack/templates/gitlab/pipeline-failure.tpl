{
   "icon_emoji": ":gitlab:",
   "attachments": [
      {
         "fallback":"View <${CI_PROJECT_URL}/-/pipelines/${CI_PIPELINE_ID}|Pipeline failure>.",
         "pretext":":fire: Failed running pipeline for *${CI_PROJECT_NAME}* from *${CI_COMMIT_REF_NAME}*",
         "color":"#FF5233",
         "fields":[
            {
               "title":"View Commit",
               "value":"<${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}|${CI_COMMIT_TITLE}>",
               "short":false
            },
            {
               "title":"View Failed Pipeline",
               "value":"<${CI_PROJECT_URL}/-/pipelines/${CI_PIPELINE_ID}|Pipeline #${CI_PIPELINE_ID}>",
               "short":false
            }
         ]
      }
   ]
}
