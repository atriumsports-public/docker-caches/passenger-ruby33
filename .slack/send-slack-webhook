#!/usr/bin/env bash

set -euo pipefail

# .NAME
#   send-slack-webhook is a curl wrapper and templating engine to seperate notification logic from CI configuration
#
# .SYNOPSIS
#   Based on a set of templates provided in ./templates, send a notification to slack (via webhooks api) to notify a team of some event (e.g. CI Pipeline failure).
#
#		The --type parameter corresponds to the template hierarchy
#		The --template parameter corresponds to a file of the pattern ``./templates/${type}/${template}.tpl`
#		The --url paramater must be a formed Slack webhook url
#
# .EXAMPLES
#   Sending a success notification to Slack from gitlab:
#			`send-slack-webhook --type gitlab --template job-success --url $SOME_WEBHOOK_URL`
#

# Global variables
CTX_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Helper Functions
function usage() {
	echo "Usage: $0 --template template-name --url https://webhooks.slack.com/ABC/DEF --type thetype [--is-private true]"
}

function parse_opts {
	while (("$#")); do
		case $1 in
			"--template")
				shift;
				template=$1
				;;
			"--type")
				shift;
				type=$1
				;;
			"--url")
				shift;
				url=$1
				;;
			"--is-private")
				shift;
				is_private=$1
				;;
			*)
				shift
		esac
	done
}

function validate() {
	set +u
	missing=()
	IFS=,; for arg in $required_params; do
		if [[ -z ${!arg} ]]; then
			missing+=($arg)
		fi
	done

	if [[ ${#missing[@]} -gt 0 ]]; then
			echo "Missing required arguments: ${missing[@]}"
			usage
			exit 1
	fi
	set -u
}

# Send the payload
function send() {
	# Workaround for dealing with envsubst not being available
	local templated_content_file=$(cat ${CTX_DIR}/templates/${type}/${template}.tpl)
	local templated_content=$(eval "cat <<-EOF
	$templated_content_file
EOF
" 2> /dev/null)

	local curl=(curl)
	# If running in a private group, show errors, otherwise never show any output
	if [ "${is_private}" != "true" ]; then
		curl=(${curl[@]} --silent --fail --output /dev/null)
	fi

	exec ${curl[@]} -L -X POST -H "Content-type: application/json" --data "$templated_content" --url $url
}

# Drive the command
function main() {
	# Options
	local template=""
	local url=""
	local is_private=""
	local type=""

	# Required options
	local required_params=(template,url,type)

	# Populate variables
	parse_opts $@

	# Validate against required options
	validate	

	# Send the payload
	send
}

main $@
